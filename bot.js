//підключаємо модуль
const TelegramBot = require('node-telegram-bot-api');
//ініціалізуємо токен
const token = "806079932:AAHPlL_pB1sIge8zQmsu3JxGwboYVa6KcTA";
//створюємо бота
const bot = new TelegramBot(token, {polling: true});
var  http = require('http');

http.createServer(function(request, response){
    response.writeHead(200, {"Content-Type": "text/plain"})
    response.end();
}).listen(process.env.PORT)

/*
const alco = [
    {
        name: "Пивасій",
        effect: "Відновлює 1хп"
    },
    {
        name:"Водочка",
        effect:"-1 до інтелекту, +1 до консти" 
    },
    {
        name:"Винішко",
        effect:"-1 до інтелекту, +1 до харизми" 
    },
];*/
// для рандому черги
const players = ["Настя#1","Настя#2","Рі","Вова"];
bot.onText(/\/order/, (msg)=>{
    var playersCopy = [...players];
    var chatId = msg.chat.id;
    var orderString = "";
    for(var i = 0; i < players.length;i++)
    {
        //рандомить індекс в масиві
        var playerIndex = Math.floor(Math.random()*playersCopy.length);
        var elem = playersCopy[playerIndex];
        orderString += elem + "\n";
        playersCopy.splice(playerIndex, 1);
    }
    bot.sendMessage(chatId,orderString);
});
/*
bot.onText(/\/alcohol/, (msg)=>{
    var chatId = msg.chat.id;
    var item = alco[Math.floor(Math.random()*alco.length)];
    var alcoString = item.name+'\n'+item.effect+'.';
    bot.sendMessage(chatId, alcoString);
});
*/

//для обчислення модифікатора для старту трігера
function countModifST(persent, hp, full){
    let count = 0;
   /* 
    console.log(full);
    console.log(hp);*/
    //рахує модифікатор для старту трігера
    do
    {
        count++;
        full -= persent;
        
    }while(full > hp);
    return count;
}
//для обчислення модифікатора завершення трігера
function countModif(persent, hp){
   
    if( typeof countModif.count == 'undefined' ) {
        countModif.count = 0;
    }
    if(countModif.count <= 20){
        countModif.count++;
        hp -= persent;
    }
    
    
    return countModif.count;
}
// імітація кидка дайсу
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max) + 1) ;
  }
// для провірки старту мод 20% < 20
function devilStart(hp, full){
    const twPersent = full * 0.2;
    var mod = countModifST(twPersent, hp, full);
    var dice = getRandomInt(20);
    /*console.log("Dice: " + dice);
    console.log(mod);
    console.log(mod >= dice);*/
    //для логування в мій діалог
    var logString ="Start. Mod: "+mod + " Dice: "+dice ;
    bot.sendMessage(282989796,logString);
    return mod>=dice? "Сатана на волі!": "Пронесло.";
}
//для провірки завершення трігера 10+мод < д20 + мод консти
function devilEnd(modif, hp){
    var dice = getRandomInt(20);
    const tenPers = hp * 0.1;
    var mod = countModif(tenPers, hp);
    var logString = "End. Mod: "+(10+ Number(mod)) + " Dice+Mod: "+(Number(dice) + Number(modif) + " Dice: "+dice);
    bot.sendMessage(282989796, logString);
    /*
    console.log(mod);
    console.log(dice);
    console.log(10+Number(mod));
    console.log(Number(dice)+Number(modif));*/
    if ((10+Number(mod))>(Number(dice)+Number(modif)))
    {
        return "Сатана продовжує бушувати!"; 
    }
    else
    {
        countModif.count = 0;
        return "Сатана зник.";
    }
    
}
bot.onText(/\/dt (.+) (.+) (.+)/, (msg, match)=>{
    var chatId = msg.chat.id;
    var out = '';
    var modif = match[2];
    const full = match[3];
    switch (match[1]){
        case 'hp':
            {
                out = devilStart(modif, full);
                
                break;
            };
        case 'mod':
            {
                if (modif > 10)
                {
                    out = "Wrong modifier!";
                    break;
                }
                out = devilEnd(modif, full);
                break;
            };
        default:
            {
                out = "Wrong params!";
                break;
            };
    };
    //для логів в мій діалог
    bot.sendMessage(282989796,out);
    bot.sendMessage(chatId,out);
});

bot.onText(/shut/,(msg)=>{
    const chatID = msg.chat.id;

    bot.sendSticker(chatID, "./sticker.webp" )

});

bot.on("polling_error", (err) => console.log(err));